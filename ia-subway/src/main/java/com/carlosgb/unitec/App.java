package com.carlosgb.unitec;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

/**
 * Hello world!
 */
public final class App {

    static JsonArray lineasArray;
    static final HashMap<String, Set<String>> map = new HashMap<String, Set<String>>();

    private static void loadInfo() {
        try {
            URL urlBase=App.class.getClass().getResource("/lineas-metro.json");
            URL urlLinea12=App.class.getClass().getResource("/linea-12.json");
            File source = new File(urlBase.toURI()),sourceLinea12=new File(urlLinea12.toURI());
            Gson gson = new Gson();
            try (FileReader fr = new FileReader(source);
            FileReader fr2 = new FileReader(sourceLinea12)) {
                JsonReader reader = new JsonReader(fr);
                JsonObject data = gson.fromJson(reader, JsonObject.class);
                reader = new JsonReader(fr2);
                JsonObject dataLinea12 = gson.fromJson(reader, JsonObject.class);
                data.get("lineas").getAsJsonArray().add(dataLinea12);
                lineasArray = data.get("lineas").getAsJsonArray();
                // System.out.println("Lineas :" + lineasArray.size());
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println("Error al cargar el json.");
            }
        } catch (Exception uex) {
            uex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        loadInfo();
        ordenarTupla();
        System.out.println("-------------------------------");
        imprimirTupla();
    }

    public static void ordenarTupla() {
        for (int i = 0; i < lineasArray.size(); i++) {
            System.out.println(lineasArray.get(i).getAsJsonObject().get("nombre").getAsString());
            JsonArray arrayEstaciones = lineasArray.get(i).getAsJsonObject().get("estaciones").getAsJsonArray();
            System.out.println("Estaciones: " + arrayEstaciones.size());
            for (int j = 0; j < arrayEstaciones.size(); j++) {
                JsonObject temp = arrayEstaciones.get(j).getAsJsonObject();
                if (!map.containsKey(temp.get("nombre").getAsString())) {
                    map.put(temp.get("nombre").getAsString(), new TreeSet<>());
                }
                if (j != 0) {
                    JsonObject estacionAnterior = arrayEstaciones.get(j - 1).getAsJsonObject();
                    map.get(temp.get("nombre").getAsString()).add(estacionAnterior.get("nombre").getAsString());
                }
                if ((j + 1) < arrayEstaciones.size()) {
                    JsonObject estacionSiguiente = arrayEstaciones.get(j + 1).getAsJsonObject();
                    map.get(temp.get("nombre").getAsString()).add(estacionSiguiente.get("nombre").getAsString());
                }
            }

        }
    }

    public static void imprimirTupla() {
        Set<String> keys = map.keySet();
        StringBuilder sb = new StringBuilder();
        for (String estacion : keys) {
            // System.out.println(estacion+":");
            StringBuilder tempSB = new StringBuilder();
            for (String temp : map.get(estacion)) {
                // System.out.println("\t"+temp);
                tempSB.append("'").append(temp).append("'").append(",");
            }
            tempSB.setLength(tempSB.length() - 1);
            sb.append("'").append(estacion).append("'");
            sb.append(":").append("{").append(tempSB).append("},\n");
        }
        System.out.println(sb);
    }
}
