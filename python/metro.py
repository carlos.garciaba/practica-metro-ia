# Vuelos con busqueda en amplitud

from arbol import Nodo


def buscar_solucion_BFS(conexiones, estado_inicial, solucion):

    solucionado = False
    nodos_visitados = []
    nodos_frontera = []
    nodoInicial = Nodo(estado_inicial)
    nodos_frontera.append(nodoInicial)
    while (not solucionado) and len(nodos_frontera) != 0:
        nodo = nodos_frontera[0]
        # Extraer todo y añadirlo a visitados
        nodos_visitados.append(nodos_frontera.pop(0))

        if nodo.get_datos() == solucion:
            # solución encontrada
            solucionado = True
            return nodo  # nodos_visitados
        else:
            # Expandir nodos hijo   (ciudades con conexion)
            dato_nodo = nodo.get_datos()
            lista_hijos = []
            for un_hijo in conexiones[dato_nodo]:
                hijo = Nodo(un_hijo)
                lista_hijos.append(hijo)
                if not hijo.en_lista(nodos_visitados)\
                        and not hijo.en_lista(nodos_frontera):
                    nodos_frontera.append(hijo)
            nodo.set_hijos(lista_hijos)


if __name__ == "__main__":
    conexiones = {
        'Atlalilco': {'Culhuacán', 'Escuadrón 201', 'Iztapalapa', 'Mexicaltzaingo'},
        'Canal del Norte': {'Consulado', 'Morelos'},
        'Ricardo Flores Magón': {'Romero Rubio', 'San Lázaro'},
        'Insurgentes': {'Cuauhtémoc', 'Sevilla'},
        'Politécnico': {'Instituto del Petróleo'},
        'Ferrería': {'Azcapotzalco', 'Norte 45'},
        'Doctores': {'Obrera', 'Salto del Agua'},
        'Azcapotzalco': {'Ferrería', 'Tezozomoc'},
        'Eugenia': {'División del Norte', 'Etiopía'},
        'Allende': {'Bellas Artes', 'Zócalo'},
        'San Lázaro': {'Candelaria', 'Moctezuma', 'Morelos', 'Ricardo Flores Magón'},
        'Aragón': {'Eduardo Molina', 'Oceanía'},
        'Pino Suárez': {'Isabel la Católica', 'Merced', 'San Antonio Abad', 'Zócalo'},
        'Juárez': {'Balderas', 'Hidalgo'},
        'Canal de San Juan': {'Agrícola Oriental', 'Tepalcates'},
        'San Juan de Letrán': {'Bellas Artes', 'Salto del Agua'},
        'El Rosario': {'Aquiles Serdán', 'Tezozomoc'},
        'Aquiles Serdán': {'Camarones', 'El Rosario'},
        'División del Norte': {'Eugenia', 'Zapata'},
        'Aculco': {'Apatlaco', 'Escuadrón 201'},
        'San Pedro de los Pinos': {'San Antonio', 'Tacubaya'},
        'Romero Rubio': {'Oceanía', 'Ricardo Flores Magón'},
        'Buenavista': {'Guerrero'},
        'Nezahualcóyotl': {'Impulsora', 'Villa de Aragón'},
        'Coyuya': {'Iztacalco', 'Santa Anita'},
        'Revolución': {'Hidalgo', 'San Cosme'},
        'Acatitla': {'Peñón Viejo', 'Santa Marta'},
        'Cuauhtémoc': {'Balderas', 'Insurgentes'},
        'Observatorio': {'Tacubaya'},
        'Mexicaltzaingo': {'Atlalilco', 'Ermita'},
        'Tepalcates': {'Canal de San Juan', 'Guelatao'},
        'Miguel Angel de Quevedo': {'Copilco', 'Viveros'},
        'Zapotitlán': {'Nopalera', 'Tlaltenco'},
        'Villa de Aragón': {'Bosque de Aragón', 'Nezahualcóyotl'},
        'Patriotismo': {'Chilpancingo', 'Tacubaya'},
        'Martín Carrera': {'La Villa-Basílica', 'Talismán'},
        'Panteones': {'Cuatro Caminos', 'Tacuba'},
        'Colegio Militar': {'Normal', 'Popotla'},
        'Insurgentes Sur': {'Hospital 20 de Noviembre', 'Mixcoac'},
        'Hangares': {'Pantitlán', 'Terminal Aérea'},
        'San Cosme': {'Normal', 'Revolución'},
        'Instituto del Petróleo': {'Autobuses del Norte', 'Lindavista', 'Politécnico', 'Vallejo'},
        'Copilco': {'Miguel Angel de Quevedo', 'Universidad'},
        'Talismán': {'Bondojito', 'Martín Carrera'},
        'Deportivo 18 de Marzo': {'Indios Verdes', 'La Villa-Basílica', 'Lindavista', 'Potrero'},
        'Jamaica': {'Chabacano', 'Fray Servando', 'Mixiuhca', 'Santa Anita'},
        'Valle Gómez': {'Consulado', 'Misterios'},
        'Autobuses del Norte': {'Instituto del Petróleo', 'La Raza'},
        'Niños Héroes': {'Balderas', 'Hospital General'},
        'Universidad': {'Copilco'},
        'Pantitlán': {'Agrícola Oriental', 'Hangares', 'Puebla', 'Zaragoza'},
        'Cuatro Caminos': {'Panteones'},
        'Olímpica': {'Ecatepec', 'Plaza Aragón'},
        'Hospital 20 de Noviembre': {'Insurgentes Sur', 'Zapata'},
        'La Viga': {'Chabacano', 'Santa Anita'},
        'La Villa-Basílica': {'Deportivo 18 de Marzo', 'Martín Carrera'},
        'Puebla': {'Ciudad Deportiva', 'Pantitlán'},
        'Misterios': {'La Raza', 'Valle Gómez'},
        'Juanacatlán': {'Chapultepec', 'Tacubaya'},
        'Lindavista': {'Deportivo 18 de Marzo', 'Instituto del Petróleo'},
        'Consulado': {'Bondojito', 'Canal del Norte', 'Eduardo Molina', 'Valle Gómez'},
        'Tepito': {'Lagunilla', 'Morelos'},
        'Centro Médico': {'Chilpancingo', 'Etiopía', 'Hospital General', 'Lázaro Cárdenas'},
        'Cuitlahuac': {'Popotla', 'Tacuba'},
        'Ermita': {'Eje Central', 'General Anaya', 'Mexicaltzaingo', 'Portales'},
        'Merced': {'Candelaria', 'Pino Suárez'},
        'Apatlaco': {'Aculco', 'Iztacalco'},
        'San Antonio': {'Mixcoac', 'San Pedro de los Pinos'},
        'Eje Central': {'Ermita', 'Parque de los Venados'},
        'Ciudad Deportiva': {'Puebla', 'Velódromo'},
        'Indios Verdes': {'Deportivo 18 de Marzo'},
        'La Paz': {'Los Reyes'},
        'Los Reyes': {'La Paz', 'Santa Marta'},
        'Tacubaya': {'Constituyentes', 'Juanacatlán', 'Observatorio', 'Patriotismo', 'San Pedro de los Pinos'},
        'Normal': {'Colegio Militar', 'San Cosme'},
        'Balderas': {'Cuauhtémoc', 'Juárez', 'Niños Héroes', 'Salto del Agua'},
        'Portales': {'Ermita', 'Nativitas'},
        'UAM-I': {'Cerro de la Estrella', 'Constitución de 1917'},
        'Popotla': {'Colegio Militar', 'Cuitlahuac'},
        'San Antonio Abad': {'Chabacano', 'Pino Suárez'},
        'Guelatao': {'Peñón Viejo', 'Tepalcates'},
        'Coyoacán': {'Viveros', 'Zapata'},
        'Auditorio': {'Constituyentes', 'Polanco'},
        'Polanco': {'Auditorio', 'San Joaquín'},
        'Parque de los Venados': {'Eje Central', 'Zapata'},
        'Obrera': {'Chabacano', 'Doctores'},
        'Santa Anita': {'Coyuya', 'Jamaica', 'La Viga'},
        'Culhuacán': {'Atlalilco', 'San Andrés Tomatlán'},
        'Tasqueña': {'General Anaya'},
        'Constituyentes': {'Auditorio', 'Tacubaya'},
        'San Joaquín': {'Polanco', 'Tacuba'},
        'Deportivo Oceanía': {'Bosque de Aragón', 'Oceanía'},
        'Potrero': {'Deportivo 18 de Marzo', 'La Raza'},
        'Agrícola Oriental': {'Canal de San Juan', 'Pantitlán'},
        'Zócalo': {'Allende', 'Pino Suárez'},
        'General Anaya': {'Ermita', 'Tasqueña'},
        'Cerro de la Estrella': {'Iztapalapa', 'UAM-I'},
        'Isabel la Católica': {'Pino Suárez', 'Salto del Agua'},
        'Impulsora': {'Nezahualcóyotl', 'Río de los Remedios'},
        'Morelos': {'Canal del Norte', 'Candelaria', 'San Lázaro', 'Tepito'},
        'Gómez Farías': {'Boulevard Puerto Aéreo', 'Zaragoza'},
        'Tlatelolco': {'Guerrero', 'La Raza'},
        'Ecatepec': {'Muzquiz', 'Olímpica'},
        'La Raza': {'Autobuses del Norte', 'Misterios', 'Potrero', 'Tlatelolco'},
        'Hidalgo': {'Bellas Artes', 'Guerrero', 'Juárez', 'Revolución'},
        'Constitución de 1917': {'UAM-I'},
        'Plaza Aragón': {'Ciudad Azteca', 'Olímpica'},
        'Camarones': {'Aquiles Serdán', 'Refinería'},
        'Calle 11': {'Lomas Estrella', 'Periférico Oriente'},
        'Salto del Agua': {'Balderas', 'Doctores', 'Isabel la Católica', 'San Juan de Letrán'},
        'Iztacalco': {'Apatlaco', 'Coyuya'},
        'Refinería': {'Camarones', 'Tacuba'},
        'Mixiuhca': {'Jamaica', 'Velódromo'},
        'Sevilla': {'Chapultepec', 'Insurgentes'},
        'Zaragoza': {'Gómez Farías', 'Pantitlán'},
        'Candelaria': {'Fray Servando', 'Merced', 'Morelos', 'San Lázaro'},
        'Chabacano': {'Jamaica', 'La Viga', 'Lázaro Cárdenas', 'Obrera', 'San Antonio Abad', 'Viaducto'},
        'Olivos': {'Nopalera', 'Tezonco'},
        'Tezozomoc': {'Azcapotzalco', 'El Rosario'},
        'Villa de Cortés': {'Nativitas', 'Xola'},
        'Peñón Viejo': {'Acatitla', 'Guelatao'},
        'Lázaro Cárdenas': {'Centro Médico', 'Chabacano'},
        'Río de los Remedios': {'Impulsora', 'Muzquiz'},
        'Bondojito': {'Consulado', 'Talismán'},
        'Tezonco': {'Olivos', 'Periférico Oriente'},
        'Lagunilla': {'Garibaldi', 'Tepito'},
        'Terminal Aérea': {'Hangares', 'Oceanía'},
        'Oceanía': {'Aragón', 'Deportivo Oceanía', 'Romero Rubio', 'Terminal Aérea'},
        'Bosque de Aragón': {'Deportivo Oceanía', 'Villa de Aragón'},
        'Escuadrón 201': {'Aculco', 'Atlalilco'},
        'Fray Servando': {'Candelaria', 'Jamaica'},
        'Nativitas': {'Portales', 'Villa de Cortés'},
        'Barranca del Muerto': {'Mixcoac'},
        'Tláhuac': {'Tlaltenco'},
        'Garibaldi': {'Bellas Artes', 'Guerrero', 'Lagunilla'},
        'Boulevard Puerto Aéreo': {'Balbuena', 'Gómez Farías'},
        'Muzquiz': {'Ecatepec', 'Río de los Remedios'},
        'Santa Marta': {'Acatitla', 'Los Reyes'},
        'Etiopía': {'Centro Médico', 'Eugenia'},
        'Moctezuma': {'Balbuena', 'San Lázaro'},
        'Eduardo Molina': {'Aragón', 'Consulado'},
        'Bellas Artes': {'Allende', 'Garibaldi', 'Hidalgo', 'San Juan de Letrán'},
        'Tacuba': {'Cuitlahuac', 'Panteones', 'Refinería', 'San Joaquín'},
        'Chilpancingo': {'Centro Médico', 'Patriotismo'},
        'Xola': {'Viaducto', 'Villa de Cortés'},
        'Hospital General': {'Centro Médico', 'Niños Héroes'},
        'Viveros': {'Coyoacán', 'Miguel Angel de Quevedo'},
        'Lomas Estrella': {'Calle 11', 'San Andrés Tomatlán'},
        'Balbuena': {'Boulevard Puerto Aéreo', 'Moctezuma'},
        'Periférico Oriente': {'Calle 11', 'Tezonco'},
        'Guerrero': {'Buenavista', 'Garibaldi', 'Hidalgo', 'Tlatelolco'},
        'Chapultepec': {'Juanacatlán', 'Sevilla'},
        'Tlaltenco': {'Tláhuac', 'Zapotitlán'},
        'Nopalera': {'Olivos', 'Zapotitlán'},
        'Iztapalapa': {'Atlalilco', 'Cerro de la Estrella'},
        'Viaducto': {'Chabacano', 'Xola'},
        'San Andrés Tomatlán': {'Culhuacán', 'Lomas Estrella'},
        'Ciudad Azteca': {'Plaza Aragón'},
        'Norte 45': {'Ferrería', 'Vallejo'},
        'Velódromo': {'Ciudad Deportiva', 'Mixiuhca'},
        'Zapata': {'Coyoacán', 'División del Norte', 'Hospital 20 de Noviembre', 'Parque de los Venados'},
        'Mixcoac': {'Barranca del Muerto', 'Insurgentes Sur', 'San Antonio'},
        'Vallejo': {'Instituto del Petróleo', 'Norte 45'}
    }

    estado_inicial = 'Tacubaya'
    solucion = 'Ermita'
    nodo_solucion = buscar_solucion_BFS(conexiones, estado_inicial, solucion)
    # mostrar resultado
    resultado = []
    nodo = nodo_solucion
    while nodo.get_padre() != None:
        resultado.append(nodo.get_datos())
        nodo = nodo.get_padre()
    resultado.append(estado_inicial)
    resultado.reverse()
    print(resultado)
